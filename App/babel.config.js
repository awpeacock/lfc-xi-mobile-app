module.exports = function (api) {
    api.cache(true)
    return {
        presets: ['module:@react-native/babel-preset'],
        plugins: [
            [
                "module-resolver",
                {
                    root: ['.'],
                    extensions: [
                        '.js',
                        '.jsx',
                        '.ts',
                        '.tsx',
                        '.android.js',
                        '.android.tsx',
                        '.ios.js',
                        '.ios.tsx'
                    ],
                    alias: {
                        "@app": ["./modules/app"],
                        "@components": ["./modules/components"],
                        "@screens": ["./modules/screens"],
                        "@styles": ["./modules/styles"],
                        "@fonts": ["./assets/fonts"],
                        "@images": ["./assets/images"]
                    }
                }
            ],
            [
                "module:react-native-dotenv"
            ]
        ]
    }
}