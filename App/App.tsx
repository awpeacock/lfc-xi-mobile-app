import React from 'react';

import mobileAds from 'react-native-google-mobile-ads';
import * as SplashScreen from 'expo-splash-screen';

import { Application, LFCProvider } from '@app';

SplashScreen.preventAutoHideAsync();

export default function App() {

    mobileAds()
        .initialize()
        .then(adapterStatuses => {
            // Initialization complete!
        });

    return (
        <LFCProvider>
            <Application />
        </LFCProvider>
    );
}