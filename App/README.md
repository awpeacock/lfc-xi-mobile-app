# Liverpool FC Hall of Fame

**Liverpool FC Hall of Fame** is a React Native app for allowing people to submit teams containing their favourite LFC players, and then displaying the most popular selections from all fans.

## Installation

Use [nodejs](https://nodejs.org/en/) and [expo](https://expo.dev) to install **Liverpool FC Hall of Fame**.  From the root directory of the codebase, pull down the latest sourcecode from the repository and run the following:

```bash
npm install
```

## Maintenance

Ensure to update dependencies regularly (and check they don't break the app) - especially as Google/Apple will regularly request updates to use the latest SDKs.

```bash
npm outdated
npx npm-check-updates -u
npm install
```

## Usage

If, for any reason (likely on upgrading React Native via npm), you need to rebuild the native implementations, you will need to "eject" the project (using the old terminology) to create a bare native project.  First delete the existing ``android`` and ``ios`` folders, and then execute the following:

```bash
npx expo prebuild
```

#### Android

If you have rebuilt using the steps above, you will need to amend your *android/app/src/main/AndroidManifest.xml*, and update the `<application>` node as below:

```xml
<application android:name=".MainApplication" android:label="@string/app_name" android:icon="@mipmap/ic_launcher" android:roundIcon="@mipmap/ic_launcher_round" android:allowBackup="true" android:theme="@style/AppTheme" android:usesCleartextTraffic="true">
```

To remove the duplicate splash screen issue introduced with Android 12, add the following line to *android/app/src/main/res/values/styles.xml* within the `Theme.App.SplashScreen` node:

```xml
<item name="android:windowIsTranslucent">true</item>
```

> You may also face issues where you need to amend *AndroidManifest.xml* to prevent startup errors when running the app, by adding the following entry:
> ```xml
> <meta-data android:name="expo.modules.updates.EXPO_UPDATE_URL" android:value="https://www.example.com" />
> ```

### Emulator Testing

Open up a ***Node*** command prompt, navigate to the root of the project, and run:

```bash
npm run-android
```

To build an APK to deploy manually to a device:

```bash
npx react-native run-android --mode=release
```

If you see the following exception running this command then you need to uninstall the app from the emulator and run `gradlew clean` from within the *android* directory:

> `INSTALL_FAILED_UPDATE_INCOMPATIBLE: Package com.redpenguin.lfcxi signatures do not match previously installed version`

The APK will be found in *android/app/build/outputs/apk/release*.

### Physical Device Testing

**NOTE** Expo Go will not work with Google Ads - these would need to be removed from the code before attempting the below.

Start up the development server using ***Expo CLI*** and scan the resulting QR code from [Expo Go](https://expo.dev/go) to run the debug version of the app on your physical device.

```bash
```npx expo start
```

### Build for Release

#### Android
Follow the instructions below (taken from [React Native Dev]https://reactnative.dev/docs/signed-apk-android) to setup a keystore and add the values to the Gradle properties.

If you don't already have a keystore, you will need to build that first:

```bash
keytool -genkey -v -keystore lfc-xi.keystore -alias lfc-xi -keyalg RSA -keysize 2048 -validity 10000
```

Copy this file into *android/app*.

Edit the Android Gradle build file *android/app/build.gradle* and update the following lines:

```json
    signingConfigs {
        ...
        release {
            if (project.hasProperty('MYAPP_UPLOAD_STORE_FILE')) {
                storeFile file(MYAPP_UPLOAD_STORE_FILE)
                storePassword MYAPP_UPLOAD_STORE_PASSWORD
                keyAlias MYAPP_UPLOAD_KEY_ALIAS
                keyPassword MYAPP_UPLOAD_KEY_PASSWORD
            }
        }
    }
```

Ensure also that the build is set to use the correct signing key for the *release* variant:

```json
    buildTypes {
        ...
        release {
            signingConfig signingConfigs.release
            ...
        }
    }
```

Then add these aliases to *android/gradle.properties*:

```properties
MYAPP_UPLOAD_STORE_FILE=lfc-xi.keystore
MYAPP_UPLOAD_KEY_ALIAS=lfc-xi
MYAPP_UPLOAD_STORE_PASSWORD=Hobby0R151T?
MYAPP_UPLOAD_KEY_PASSWORD=Hobby0R151T?
```

Ensure the following config is set to `true` in *android/app/build.gradle* to ensure an optimal build size for your app:

```json
def enableProguardInReleaseBuilds = (findProperty('android.enableProguardInReleaseBuilds') ?: true).toBoolean()
```

Finally, the following command will build the AAB file for uploading to the Google Play Store:

```bash
cd android
gradlew bundleRelease
```

The AAB will be found in *android\/app/build/outputs/bundle/release*.

## Roadmap

- Add modal for "rules" (explanations as to when players become eligible for selection)
- Graphical interface for player selection (shirt icons laid out in 4-4-2 formation)
- Allow submissions by users for players to include
- Link from players to their [lfchistory.net](https://www.lfchistory.net) profile (or allow contributions to add player profiles)

## Author

Alasdair Peacock