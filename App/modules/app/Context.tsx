import React from 'react';
import { Alert, BackHandler, Linking, Platform } from 'react-native';

import { Asset } from 'expo-asset';
import VersionCheck from 'react-native-version-check';

import * as Model from 'lfc-common/model';
import { Nullable } from 'lfc-common/model';

interface LFCContextType {
    loaded: {
        assets: boolean,
        submission: boolean,
        month: boolean
    };

    latest: boolean;

    assets: Record<string, Asset>;

    positions: Record<string, Model.Position.Group>;
    formation: Nullable<Model.Formation>;
    submission: Nullable<Model.Team>;
    month: Nullable<Model.Team>;

    message: string;
    error: Nullable<Record<string, any>>;

    setSubmission: (team: Model.Team, message: string) => void;
}

interface LFCContextProps {
    children: React.ReactNode
}
  
export const LFCContext = React.createContext<Nullable<LFCContextType>>(null);

export const LFCProvider: React.FC<LFCContextProps> = ({ children }) => {

    const [context, setContext] = React.useState<LFCContextType>({

        loaded: {
            assets: false,
            submission: false,
            month: false
        },

        latest: false,

        assets: {},

        positions: {},
        formation: null,
        submission: null,
        month: null,

        message: '',
        error: null,

        setSubmission: (team: Model.Team, message: string): void => setContext(context => ({
            ...context,
            submission: team,
            message: message
        }))

    });

    const aws: string = 'https://33j8oxf66j.execute-api.eu-west-2.amazonaws.com/latest';
    const domain: string = 'http://www.liverpoolfcxi.co.uk/restful/';

    const checkUpdates = async (): Promise<void> => {
        const url: string = aws + '/version';
        const minimum: string = await fetch(url).then(res => res.text());
        const current: string = VersionCheck.getCurrentVersion();
        let store: string = '';
        if ( Platform.OS === 'android' ) {
            store = await VersionCheck.getPlayStoreUrl();
        } else {
            store = await VersionCheck.getAppStoreUrl();
        }
        if ( minimum > current ) {
            Alert.alert(
                'Update needed', 
                'You need to update to the latest version of the app to continue using with the latest features',
                [
                    {
                        text: 'Update',
                        onPress: () => {
                            BackHandler.exitApp();
                            Linking.openURL(store);
                        }
                    }
                ],
                { cancelable: false }
            );
        } else {
            setContext(context => ({
                ...context,
                latest: true
            }));
        }
    }
        
    const loadAsset = async (id: string, file: number): Promise<void> => {
        const asset: Asset = await Asset.fromModule(file).downloadAsync();
        let assets: Record<string, Asset> = context.assets ?? {};
        assets[id] = asset;
        setContext(context => ({
            ...context,
            loaded: {
                ...context.loaded,
                assets: (Object.entries(assets).length == 6)
            },
            assets: assets
        }));
    }

    const loadSubmission = async (): Promise<void> => {
        await fetch(domain + 'formation')
            .then(res => res.json())
            .then(
                (result) => {
                    if ( result.players ) {
                        const team: Model.Team = Model.Team.fromJson(result);
                        context.setSubmission(team, 'It looks like you\'ve already submitted a team for this month - thanks. Check back next month to see the overall results.');
                    } else {
                        const positions: Record<Model.Position.ID, Model.Position.Group> = {} as Record<Model.Position.ID, Model.Position.Group>;
                        result.formation.positions.forEach((json) => {
                            const position: Model.Position.Group = new Model.Position.Group(json.initials as Model.Position.ID);
                            json.players.forEach((p) => {
                                if ( p.id > 0 && p.name != '' ) {
                                    const player: Model.Player = Model.Player.fromJson(p);
                                    position.add(player);
                                }
                            })
                            positions[position.id] = position;
                        });
                        setContext(context => ({
                            ...context,
                            formation: Model.Formation.fromJson(result.formation),
                            positions: positions 
                        }));
                    }
                    setContext(context => ({
                        ...context,
                        loaded: {
                            ...context.loaded,
                            submission: true
                        }
                    }));
            },
                (error) => {
                    addError('submission', error);
                    setContext(context => ({
                        ...context,
                        loaded: {
                            ...context.loaded,
                            submission: true
                        }
                    }));
                }
            );     
    }

    const loadTeamOfTheMonth = async (): Promise<void> => {
        await fetch(domain + 'month')
            .then(res => res.json())
            .then(
                (result) => {
                    const team: Model.Team = Model.Team.fromJson(result);
                    setContext(context => ({
                        ...context,
                        loaded: {
                            ...context.loaded,
                            month: true
                        },
                        month: team
                    }));
                },
                (error) => {
                    addError('month', error);
                    setContext(context => ({
                        ...context,
                        loaded: {
                            ...context.loaded,
                            month: true
                        }
                    }));
                }
            );   
    }

    const addError = (feature: string, error: any): void => {
        let err: Nullable<Record<string, any>> = context.error ?? {};
        err[feature] = error;
        setContext(context => ({
            ...context,
            error: err
        }));
    }

    React.useEffect(() => {
        (async function() {
            await checkUpdates();

            await loadAsset('background', require('@images/layout/background.jpg'));
            await loadAsset('crest', require('@images/layout/crest.png'));
            await loadAsset('error', require('@images/layout/error.png'));
            await loadAsset('hero.submission', require('@images/buttons/hero/teamsheet.png'));
            await loadAsset('icon.calendar', require('@images/buttons/icons/calendar.png'));
            await loadAsset('icon.trophy', require('@images/buttons/icons/trophy.png'));
            
            await loadSubmission();
            await loadTeamOfTheMonth();
        })();
    }, []);

    const ready: boolean = context.loaded.assets && context.loaded.submission && context.loaded.month && context.latest;
    if ( ready ) {
        return (
            <LFCContext.Provider value={context}>
                {children}
            </LFCContext.Provider>
        );
    }
        
}