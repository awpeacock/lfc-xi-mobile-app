import React, { ReactElement } from 'react';
import { ImageBackground, View } from 'react-native';

import { NavigationContainer, Theme as NavigationTheme } from '@react-navigation/native';
import { StackNavigationOptions, createStackNavigator } from '@react-navigation/stack';
import { PaperProvider } from 'react-native-paper';

import { LFCContext } from './Context';
import { Splash } from './Splash';

import { Config } from 'lfc-common/model';
import * as Screens from '@screens';
import { GoogleAdsBanner, NavigatorHeader } from '@components/layout';
import { Stylesheet, Theme } from '@styles';

export const Application: React.FC = (): ReactElement => {

    const Stack = createStackNavigator();
    const {assets} = React.useContext(LFCContext)!;

    const navigation = {
        theme: {
            colors: {
                background: 'rgba(255,255,255,0.5)'
            }
        } as NavigationTheme,
        options: {
            headerStyle: {
                backgroundColor: '#a00',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            }
        } as StackNavigationOptions
    }

    return (
        <Splash>
            <PaperProvider theme={Theme}>
                <ImageBackground source={{uri: assets['background'].uri}}>
                    <NavigationContainer theme={navigation.theme}>
                        <View style={Stylesheet.layout.screen.full}>
                            <Stack.Navigator initialRouteName="Home" screenOptions={navigation.options}>
                                <Stack.Screen
                                    name='home'
                                    component={Screens.Home}
                                    options={{ headerTitle: props => <NavigatorHeader />, animationEnabled: false }}
                                />
                                <Stack.Screen 
                                    name='submission'
                                    component={Screens.SubmitYourTeam} 
                                    options={{ headerTitle: props => <NavigatorHeader title={Config.titles.submission} />, animationEnabled: false }}
                                />
                                <Stack.Screen 
                                    name='month'
                                    component={Screens.TeamOfTheMonth} 
                                    options={{ headerTitle: props => <NavigatorHeader title={Config.titles.month} />, animationEnabled: false }}
                                />
                                <Stack.Screen 
                                    name='hall' 
                                    component={Screens.HallOfFame} 
                                    options={{ headerTitle: props => <NavigatorHeader title={Config.titles.hall} />, animationEnabled: false }}
                                />
                            </Stack.Navigator>
                        </View>
                        <GoogleAdsBanner />
                    </NavigationContainer>
                </ImageBackground>
            </PaperProvider>
        </Splash>
    );
}