import React, { ReactElement } from 'react';
import { Animated, Image, View } from 'react-native';

import { Asset } from 'expo-asset';
import * as SplashScreen from 'expo-splash-screen';

import { Stylesheet } from '@styles';

export const Splash: React.FC<React.PropsWithChildren> = ({children}): ReactElement => {

    const [animated, setAnimated] = React.useState<boolean>(false);
    const [ready, setReady] = React.useState<boolean>(false);

    const animation = React.useMemo(() => new Animated.Value(1), []);
    const splash = require('@images/layout/splash.png');

    React.useEffect(() => {
        (async function() {
            await Asset.fromModule(splash).downloadAsync();
        })();
    }, []);
    
    const onImageLoaded = async(): Promise<void> => {
        await SplashScreen.hideAsync();
        setReady(true);
        Animated.timing(animation, {
            toValue: 0,
            duration: 1000,
            useNativeDriver: true,
        }).start(() => 
            {
                setAnimated(true);
            }
        );
    }

    return (
        <View style={{ flex: 1 }}>
            {ready && children}
            {!animated && (
                <Animated.View pointerEvents='none' style={[...Stylesheet.layout.splash.container, {opacity: animation}]}>
                    <Image style={Stylesheet.layout.splash.image} source={splash} onLoadEnd={() => { onImageLoaded() }} />
                </Animated.View>
            )}
        </View>
    )

}
