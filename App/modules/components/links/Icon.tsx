import React, { ReactElement } from 'react';
import { TouchableOpacity, Image } from 'react-native';

import { NavigationProp, ParamListBase } from '@react-navigation/native';
import { Button } from 'react-native-paper';

import { Config } from 'lfc-common/model';
import { LFCContext } from '@app/Context';
import { Stylesheet } from '@styles';

type IconProps = {
    navigation: NavigationProp<ParamListBase>,
    link: 'month'|'hall';
}

export const Icon: React.FC<IconProps> = ({navigation, link}): ReactElement => {

    const {assets} = React.useContext(LFCContext)!;
    
    let icon: string;
    switch ( link ) {
        case 'month':
            icon = assets['icon.calendar'].uri;
            break;
        case 'hall':
            icon = assets['icon.trophy'].uri;
            break;
    }
  
    return (
        <TouchableOpacity onPress={() => navigation.navigate(link)} style={Stylesheet.components.links.icon.container}>
            <Image source={{uri: icon}} style={Stylesheet.components.links.icon.image} />
            <Button mode='contained' style={Stylesheet.components.links.icon.button} labelStyle={Stylesheet.components.links.icon.text}>{Config.titles[link]}</Button>
        </TouchableOpacity>
    );
}