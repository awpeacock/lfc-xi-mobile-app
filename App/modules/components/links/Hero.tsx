import React, { ReactElement } from 'react';

import { NavigationProp, ParamListBase } from '@react-navigation/native';
import { Card, Button } from 'react-native-paper';

import { Config } from 'lfc-common/model';
import { LFCContext } from '@app/Context';
import { Stylesheet } from '@styles';

type HeroProps = {
    navigation: NavigationProp<ParamListBase>,
    link: 'submission';
}

export const Hero: React.FC<HeroProps> = ({navigation, link}): ReactElement => {

    const {assets} = React.useContext(LFCContext)!;
    
    let image: string;
    switch ( link ) {
        case 'submission':
            image = assets['hero.submission'].uri;
            break;
    }
    
    return (
        <Card mode='outlined' style={Stylesheet.components.links.hero.container} onPress={() => navigation.navigate(link)}>
            <Card.Cover source={{uri: image}} style={Stylesheet.components.links.hero.image} />
            <Card.Content style={Stylesheet.components.links.hero.button}>
                <Button mode='text' textColor='#fff' labelStyle={Stylesheet.components.links.hero.button}>{Config.titles[link]}</Button>
            </Card.Content>
        </Card>
    );
}