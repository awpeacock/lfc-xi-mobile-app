import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Text } from 'react-native-paper';

import * as Model from 'lfc-common/model';
import { Stylesheet } from '@styles';

type PlayerProps = {
    variant: 'team'|'hall',
    player: Model.Player
    position?: Model.Position.Group
}

export const Player: React.FC<PlayerProps> = ({variant, player, position}): ReactElement => {

    switch (variant) {
        case 'team':
        {
            return (
                <View style={Stylesheet.components.team.player.container}>
                    <Text variant='titleMedium' style={Stylesheet.components.team.player.position}>
                        {Model.Position.ID[position!.id]}
                    </Text>
                    <Text variant='titleMedium' style={Stylesheet.components.team.player.name}>
                        {player.forename} {player.surname}
                    </Text>
                </View>
            )
        }
        case 'hall':
        {
            return (
                <View>
                    <Text variant='titleMedium'>{player.forename} {player.surname}</Text>
                    <Text style={Stylesheet.components.hall.player.positions}>{player.positions.join(', ')}</Text>
                </View>
            )   
        }
    }

}