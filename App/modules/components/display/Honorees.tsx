import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Player } from './Player';
import { Ranking } from './Ranking';

import * as Model from 'lfc-common/model';

import { Stylesheet } from '@styles';

interface HonoreesProps {
    honorees: Model.HallOfFame
}

export const Honorees: React.FC<HonoreesProps> = ({honorees}): ReactElement => {

    return (
        <View style={Stylesheet.layout.body}>
        {
            honorees.rankings.map((ranking, r) => (
                <Ranking key={r} rank={r} votes={ranking.votes}>
                {
                    ranking.players.map((player, p) => (
                        <Player key={p} variant='hall' player={player} />
                    ))
                }
                </Ranking>
            ))
        } 
        </View>
      );
}