import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Player } from './Player';

import * as Model from 'lfc-common/model';
import { Nullable } from 'lfc-common/model';
import { Stylesheet } from '@styles';

export type TeamProps = {
    team: Nullable<Model.Team>
}

export const Team: React.FC<TeamProps> = ({team}): ReactElement => {

    if ( team == null ) {
        return <></>;
    }

    return (
        <View style={Stylesheet.components.team.container}>
        {
            team.formation.positions.map((initials, p) => {
                const position: Model.Position.Group = new Model.Position.Group(initials);
                return (
                    <Player key={p} variant='team' position={position} player={team.lineup[p].player} />
                )
            })
        }                     
        </View>
    )

}