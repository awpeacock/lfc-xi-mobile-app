import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Text } from 'react-native-paper';

import { Stylesheet } from '@styles';

interface RankingProps extends React.PropsWithChildren {
    rank: number,
    votes: number
}

export const Ranking: React.FC<RankingProps> = ({rank, votes, children}): ReactElement => {

    return (
        <View style={Stylesheet.layout.body}>
            <Text variant='titleMedium' style={[Stylesheet.components.hall.ranking.common, Stylesheet.components.hall.ranking.header]}>
                {votes} votes
            </Text>
            <View style={Stylesheet.components.hall.ranking.container}>
                {children}
            </View>
            <View style={[Stylesheet.components.hall.ranking.common, Stylesheet.components.hall.ranking.footer]} />
        </View>
    )

}