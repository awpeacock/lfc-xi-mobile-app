import React, { ReactElement } from 'react';
import { View } from 'react-native';

import ContentLoader, { Rect } from 'react-content-loader/native';

import { Stylesheet, Theme } from '@styles';

type PlayerProps = {
    count?: number
}

export const Player: React.FC<PlayerProps> = ({count = 1}): ReactElement => {

    let players: Array<ReactElement<Rect>> = [];
    for ( let p = 0; p < count; p++ ) {
        const name: ReactElement<Rect> = <Rect key={'name'+p} x={0} y={20 + (p*60)} rx={5} ry={5} width={125} height={13} />;
        const positions: ReactElement<Rect> = <Rect key={'pos'+p} x={0} y={40 + (p*60)} rx={5} ry={5} width={250} height={10} />;
        players.push(name);
        players.push(positions);
    }

    return (
        <View style={Stylesheet.layout.body}>
            <ContentLoader
                style={Stylesheet.components.skeleton.rank}
                height={Theme.fonts.titleMedium.fontSize}
                speed={1.5}
                backgroundColor={'#999'}
                foregroundColor={'#ccc'}>
                <Rect x={0} y={0} rx={5} ry={5} width={75} height={Theme.fonts.titleMedium.fontSize} />
            </ContentLoader>
            <ContentLoader
                style={Stylesheet.components.skeleton.players}
                height={10+(60*count)}
                speed={1.5}
                backgroundColor={'#999'}
                foregroundColor={'#ccc'}>
                {players}
            </ContentLoader>
        </View>
    );
}