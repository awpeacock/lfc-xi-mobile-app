import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Player } from "./Player";

import { Stylesheet } from '@styles';

export const Team: React.FC = (): ReactElement => {
    
    return (
        <View style={Stylesheet.layout.body}>
            <Player />
            <Player count={4} />
            <Player count={4} />
            <Player count={2} />
        </View>
    )

}