import React, { ReactElement, RefObject } from 'react';
import { View, TextInput as NativeInput, Keyboard } from 'react-native';

import { TextInput } from 'react-native-paper';
import { useFormikContext } from 'formik';
import { Picker } from '@react-native-picker/picker';

import * as Model from 'lfc-common/model';
import { Nullable } from 'lfc-common/model';
import { Stylesheet } from '@styles';

type TextFieldProps = {
    name: string,
    placeholder: string,
    value?: string,
    onChange: (value: string) => void,
    variant?: 'email',
    returnKey?: 'next'|'done',
    next?: RefObject<NativeInput|Picker<Nullable<Model.Player>>>
}

export const TextField = React.forwardRef<NativeInput, TextFieldProps>(({name, placeholder, value, onChange, variant, returnKey = 'done', next}, ref): ReactElement => {

    const [text, setText] = React.useState(value);

    const {setFieldTouched} = useFormikContext();

    return (
        <View>
            <TextInput 
                ref={ref}
                mode='outlined'
                style={Stylesheet.components.form.input.value}
                outlineStyle={Stylesheet.components.form.input.outline}
                placeholder={placeholder} placeholderTextColor='#aaa'
                value={text} textColor='#000' 
                onChangeText={(value) => {setText(value); onChange(value);}}
                onFocus={() => setFieldTouched(name, false)}
                onBlur={() => setFieldTouched(name)}
                autoCapitalize='none' autoComplete={variant ? variant : 'off'} keyboardType={variant =='email' ? 'email-address' : 'default'}
                returnKeyType={returnKey}
                onSubmitEditing={() => {(next ? next.current!.focus() : Keyboard.dismiss())}}
                blurOnSubmit={false}
            />
        </View>
    );

})