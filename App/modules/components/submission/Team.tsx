import React, { ForwardedRef, ReactElement, useContext } from 'react';
import { View } from 'react-native';

import { Text } from 'react-native-paper';
import { Picker } from '@react-native-picker/picker';

import { Player } from './Player';
import { FieldError } from './FieldError';

import * as Model from 'lfc-common/model';
import { Nullable } from 'lfc-common/model';
import { LFCContext } from '@app/Context';
import { Stylesheet } from '@styles';

export type TeamProps = {
    name: string,
    formation: Model.Formation,
    squad: Array<Model.Selection>,
    onChange: (field: string, value: Nullable<Model.Player>) => void
}

export const Team = React.forwardRef<Picker<Nullable<Model.Player>>, TeamProps>(({name, formation, squad, onChange}, ref): ReactElement => {

    const context = useContext(LFCContext);

    return (
        <View>
            <Text variant='titleMedium' style={Stylesheet.components.form.legend}>Team Selection</Text>
            {
                formation.positions.map((initials, p) => {
                    const refProp: Record<string, ForwardedRef<Picker<Nullable<Model.Player>>>> = {}
                    if ( p == 0 ) {
                        refProp.ref = ref;
                    }
                    return (
                        <View key={p}>
                            <Player {...refProp} name={name} id={p} position={context!.positions[initials]} selected={squad[p].player} onChange={onChange} />
                            <FieldError field={name+'['+p+']'} />
                        </View>
                    )
                })
            }                     
        </View>
    )

})