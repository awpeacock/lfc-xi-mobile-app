import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Text } from 'react-native-paper';
import { useFormikContext } from 'formik';

import { Stylesheet } from '@styles';

type FieldErrorProps = {
    field: string
}

export const FieldError: React.FC<FieldErrorProps> = ({field}): ReactElement => {

    const {errors, touched} = useFormikContext();

    if ( !errors ) {
        return <></>;
    }

    let isTouched: boolean = touched[field];
    let index: number = -1;
    if ( field.startsWith('lineup') ) {
        index = parseInt(field.match(/lineup\[(\d+)\]/)![1]);
        if ( touched['lineup'] && touched['lineup'][index] ) {
            isTouched = touched['lineup'][index].player.id;
        }
        
    }
    if ( !isTouched ) {
        return <></>;
    }
    
    let error = errors[field];
    if ( !error && field.startsWith('lineup') ) {
        let fields: Array<any> = errors['lineup'];
        if ( fields ) {
            if ( fields[index] ) {
                if ( typeof fields[index] === 'string' || fields[index] instanceof String ) {
                    error = fields[index];
                } else {
                    error = fields[index].player.id;
                }
            }
        }
    }
    if ( !error ) {
        return <></>;
    }

    return (
        <View>
            <View style={[Stylesheet.components.form.error.pointer.triangle, Stylesheet.components.form.error.pointer.outline]} />
            <View style={[Stylesheet.components.form.error.pointer.triangle, Stylesheet.components.form.error.pointer.fill]} />
            <Text variant='bodyMedium' style={Stylesheet.components.form.error.field}>{error}</Text>
        </View>
    )
}