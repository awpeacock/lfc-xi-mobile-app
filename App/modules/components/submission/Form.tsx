import React, { ReactElement, RefObject } from 'react';
import { View, TextInput as NativeInput } from 'react-native';

import { ActivityIndicator, Button, Text } from 'react-native-paper';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Picker } from '@react-native-picker/picker';

import { TextField } from './TextField';
import { Team } from './Team';
import { FieldError } from './FieldError';

import * as Model from 'lfc-common/model';
import { Nullable } from 'lfc-common/model';
import { LFCContext } from '@app/Context';
import { Status } from '@components/layout';
import { Stylesheet, Theme } from '@styles';

declare module 'yup' {
    interface UniquePlayerSchema {
        unique(message: string): UniquePlayerSchema;
    }
}

Yup.addMethod(Yup.object, 'unique', function (message) {
    return this.test('unique', message, function (value) {
        const player: Model.Player = value.player;
        const options: Array<Model.Selection> = this.parent;
        const index: number = (this.options! as any).index;
        if ( player.id == null ) {
            return true;
        }
        for ( let p = 0; p < 11; p++ ) {
            if ( p == index ) {
                continue;
            }
            if ( options[p].player.id == player.id ) {
                return this.createError();
            }
        }
        return true;
    });
});

//TODO - Strip out special chars
const SubmissionSchema: Yup.AnyObjectSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Your team name is too short')
        .max(50, 'Your team name is too long')
        .required('You must enter a team name'),
    email: Yup.string()
        .email('You have entered an invalid email address')
        .required('You must enter an email address'),
    lineup: Yup.array().of(
        (Yup.object().shape({
            player: Yup.object().shape({
                id: Yup.number()
                    .required('You must select players for every position')
                    .min(1, 'You must select players for every position')
                    
            })
        }) as any).unique('You cannot pick players more than once'))
});

type FormProps = {
    entry: Model.Submission
}

export const Form: React.FC<FormProps> = ({entry}): ReactElement => {

    const context = React.useContext(LFCContext);

    const [submission] = React.useState(entry);
    const email: RefObject<NativeInput> = React.createRef<NativeInput>();
    const team: RefObject<Picker<Nullable<Model.Player>>> = React.createRef<Picker<Nullable<Model.Player>>>();

    return (
        <Formik

            initialValues = {{
                name: submission.team.name,
                email: submission.email,
                lineup: submission.team.lineup,
                created: submission.team.created
            }}

            //validationSchema={SubmissionSchema}

            onSubmit = {async (values, {setStatus, setErrors}) => {
                submission.team.name = values.name;
                submission.email = values.email;
                submission.team.lineup = values.lineup;

                try {
                    const success = await submission.save();
                    
                    if ( success ) {
                        const message: string = 'Thanks for submitting your team.  Check back next month to see the results.';
                        values.created = submission.team.created;
                        context!.setSubmission(submission.team, message);
                        setStatus({
                            submission: {
                                state: 'success',
                                message: message
                            }
                        });
                    } else {
                        setErrors(submission.errors);
                    }
                } catch (e) {
                    setStatus({
                        submission: {
                            state: 'error',
                            message: e
                        }
                    });
                }
            }}>

            {({setFieldValue, handleChange, handleSubmit, isSubmitting, values}) => (                
                <View style={Stylesheet.components.form.container}>
                    <Status />
                    {isSubmitting && (
                        <ActivityIndicator size={100} animating={true} style={Stylesheet.components.form.spinner} />
                    )}
                    {!values.created && !isSubmitting && (
                        <View>
                            <Text variant='titleMedium' style={Stylesheet.components.form.legend}>Your Details</Text>
                            <TextField name='name' placeholder='Team Name' returnKey='next' next={email} value={values.name} onChange={handleChange('name')} />
                            <FieldError field='name' />
                            <TextField ref={email} name='email' placeholder='Email Address' variant='email' returnKey='next' next={team} value={values.email} onChange={handleChange('email')} />
                            <FieldError field='email' />
                            <Team ref={team} name='lineup' formation={submission.team.formation} squad={values.lineup} onChange={setFieldValue} />
                            <Button mode='contained' onPress={() => {handleSubmit();}}>Submit</Button>
                        </View>
                    )}
                </View>
            )}

        </Formik>
    )

}
