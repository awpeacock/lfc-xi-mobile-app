import React, { ReactElement } from 'react';
import { Keyboard, View } from 'react-native';

import { Text } from 'react-native-paper';
import { Picker } from '@react-native-picker/picker';
import { useFormikContext } from 'formik';

import { Nullable } from 'lfc-common/model';
import * as Model from 'lfc-common/model';
import { Stylesheet } from '@styles';

type PlayerProps = {
    name: string,
    id: number,
    position: Model.Position.Group,
    selected?: Nullable<Model.Player>,
    onChange: (field: string, value: Nullable<Model.Player>) => void
}

export const Player = React.forwardRef<Picker<Nullable<Model.Player>>, PlayerProps>(({name, id, position, selected, onChange}, ref): ReactElement => {

    const [player, setPlayer] = React.useState(selected);

    const {setFieldTouched} = useFormikContext();
    
    return (
        <View style={Stylesheet.components.form.player.container}>
            <View style={Stylesheet.components.form.player.picker}>
                <Picker
                    ref={ref}
                    prompt={Model.Position.ID[position.id]}
                    selectedValue={player}
                    onValueChange={(value) => { setPlayer(value); onChange(name+'['+id+'].player', value);}}
                    onFocus={() => Keyboard.dismiss()}
                    onBlur={() => setFieldTouched(name+'.'+id+'.player.id')}
                >
                <Picker.Item label='' value={-1} key={-1} />
                {
                    position.players.map((player, p) => (
                        <Picker.Item label={player.forename + ' ' + player.surname} value={player} key={p} />
                    ))
                }
                </Picker>
            </View>
            {(player == null || player.id == null) && <Text style={Stylesheet.components.form.player.placeholder}>{Model.Position.ID[position.id]}</Text>}
        </View>
    )

})