import React, { ReactElement} from 'react';
import { View, ScrollView } from 'react-native';

import { Stylesheet } from '@styles';

export const Screen: React.FC<React.PropsWithChildren> = ({children}): ReactElement => {

    return (
        <View style={Stylesheet.layout.screen.container}>
            <ScrollView contentContainerStyle={[Stylesheet.layout.screen.scroll, Stylesheet.layout.screen.content]}>
                {children}
            </ScrollView>
        </View>
    );

}