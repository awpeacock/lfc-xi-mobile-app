import React, { ReactElement } from 'react';
import { View, Image } from 'react-native';

import { Text } from 'react-native-paper';

import { LFCContext } from '@app/Context';
import { Stylesheet } from '@styles';

interface NavigatorHeaderProps {
    title?: string
}

export const NavigatorHeader: React.FC<NavigatorHeaderProps> = ({title = 'Liverpool FC Hall of Fame'}): ReactElement => {

    const {assets} = React.useContext(LFCContext)!;
    
    let styles = [Stylesheet.layout.nav.icon.common];
    if ( title != 'Liverpool FC Hall of Fame' ) {
        styles.push(Stylesheet.layout.nav.icon.sub);
    }

    return (
        <View style={Stylesheet.layout.nav.container}>
            <Image source={{uri: assets['crest'].uri}} style={styles} />
            <Text variant='titleLarge' style={Stylesheet.layout.nav.title}>{title}</Text>
        </View>
    );
}