import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { BannerAd, BannerAdSize, TestIds } from 'react-native-google-mobile-ads';
import * as Device from 'expo-device';

import { Stylesheet } from '@styles';

export const GoogleAdsBanner: React.FC = (): ReactElement => {

    const testId: string = TestIds.BANNER;
    const productionId: string = 'ca-app-pub-6557506456805358/9005776496';
    const isDevice: boolean = (Device.isDevice && !__DEV__);
    const id: string = (isDevice ? productionId : testId)

    return (
        <View style={Stylesheet.components.ad}>
            <BannerAd unitId={id} size={BannerAdSize.BANNER} />
        </View>
    );

}