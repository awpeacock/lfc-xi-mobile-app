import React, { ReactElement } from 'react';
import { View, Image } from 'react-native';

import { Status } from './Status';

import { LFCContext } from '@app/Context';
import { Stylesheet } from '@styles';

export const Error: React.FC = (): ReactElement => {

    const {assets} = React.useContext(LFCContext)!;

    return (
        <View style={Stylesheet.components.error.container}>
            <Image source={{uri: assets['error'].uri}} style={Stylesheet.components.error.image}  />
            <Status state='error' message="Sorry, it looks like we've scored an own goal and something isn't working. Please try again later." />
        </View>
    );

}