import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Icon, Text } from 'react-native-paper';
import { useFormikContext } from 'formik';

import { LFCContext } from '@app/Context';
import { Stylesheet, Theme } from '@styles';

interface StatusProps {
    state?: 'success'|'error'|'info',
    message?: string
}

export const Status: React.FC<StatusProps> = ({state, message}): ReactElement => {

    const render = (state: string, message:string) => {
        const icon: string = (state == 'success' ? 'thumb-up' : (state == 'info' ? 'information' : 'alert'));
        return (
            <View style={[Stylesheet.components.status.container, Stylesheet.components.status[state]]}>
                <Icon source={icon} size={40} color={Theme.colors[state]} />
                <Text variant='bodyMedium' style={Stylesheet.components.status.text} theme={{colors: {onSurface: Theme.colors[state]}}}>
                    {message}
                </Text>
            </View>
        );
    }

    if ( state && message ) {
        return render(state, message);
    }

    const {status} = useFormikContext();
    if (!!status && status['submission']) {
        return render(status.submission.state, status.submission.message);
    }

    const context = React.useContext(LFCContext);
    if ( context && context.submission ) {
        return render('info', context.message);
    }

    return <></>
}