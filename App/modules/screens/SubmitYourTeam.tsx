import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Text } from 'react-native-paper';

import * as Model from 'lfc-common/model';
import { Nullable } from 'lfc-common/model';
import { LFCContext } from '@app/Context';
import { Screen, Error } from '@components/layout';
import * as Submission from '@components/submission';
import * as Display from '@components/display';
import { Stylesheet } from '@styles';

export const SubmitYourTeam: React.FC = (): ReactElement => {

    const context = React.useContext(LFCContext);

    let submission: Nullable<Model.Submission>;
    if ( context!.submission != null ) {
        submission = new Model.Submission(context!.submission.formation);
        submission.team = context!.submission;
    } else if ( context!.formation != null ) {
        submission = new Model.Submission(context!.formation as Model.Formation);
    } else {
        submission = null;
    }
    
    return (
        <Screen>
            {(context!.submission || context!.formation) && (
                <View style={Stylesheet.layout.body}>
                    <Text variant='bodyMedium'>This is your chance to have your say on the greatest players to have put on the red shirts of Liverpool Football Club. We will need a team name off you (think Fantasy Football) and your email address.</Text>
                    <Submission.Form entry={submission as Model.Submission} />
                    <Display.Team team={context!.submission} />
                </View>
            )}
            {!context!.submission && !context!.formation && <Error />}
        </Screen>
    );

}