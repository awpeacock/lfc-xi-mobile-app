import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { NavigationProp, ParamListBase } from '@react-navigation/native';
import { Text } from 'react-native-paper';

import { Screen } from '@components/layout';
import * as Link from '@components/links';
import { Stylesheet } from '@styles';

type HomeProps = {
    navigation: NavigationProp<ParamListBase>
}

export const Home: React.FC<HomeProps> = ({navigation}): ReactElement => {

    return (
        <Screen>
            <View style={Stylesheet.layout.home.container}>
                <Text variant='bodyMedium'>
                    Welcome to the Liverpool FC Hall of Fame.  Select your favourite former players from the club's illustrious history, and compare your selection to that of your fellow fans.
                </Text>
                
                <Link.Hero navigation={navigation} link='submission' />
                <View style={Stylesheet.layout.home.icons}>
                    <Link.Icon navigation={navigation} link='month' />
                    <Link.Icon navigation={navigation} link='hall' />
                </View>
            </View>
        </Screen>
    );

}