import React, { ReactElement } from 'react';
import { View } from 'react-native';

import { Text } from 'react-native-paper';
import { format } from "date-fns";

import * as Model from 'lfc-common/model';
import { LFCContext } from '@app/Context';
import { Screen, Error } from '@components/layout';
import * as Display from '@components/display';
import { Stylesheet } from '@styles';

export const TeamOfTheMonth: React.FC = (): ReactElement => {

    const context = React.useContext(LFCContext);

    let current = new Date;
    current.setMonth(current.getMonth()-1);
    const month = format(current, 'MMMM yyyy');

    let message: string;
    if ( context!.month != null && context!.month.created.getTime() < 200000) {
        message = 'There were no votes cast last month. For now, content yourself with this - the squad that won Liverpool\'s first ever league title in 1900-01:'
    } else {
        message = 'The most popular players voted for last month by position.';
    }

    return (
        <Screen>
            {context!.month && (
                <View style={Stylesheet.layout.body}>
                    <Text variant='displayLarge' style={Stylesheet.layout.screen.heading}>{month}</Text>
                    <Text variant='bodyMedium' style={Stylesheet.layout.body}>{message}</Text>
                    <Display.Team team={context!.month as Model.Team}  />
                </View>
            )}
            {!context!.month && <Error />}
        </Screen>
    );

}