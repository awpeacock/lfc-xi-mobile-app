import React, { ReactElement } from 'react';

import { Text } from 'react-native-paper';

import * as Model from 'lfc-common/model';
import * as Display from '@components/display';
import * as Skeleton from '@components/skeleton';
import { Screen, Error } from '@components/layout';
import { Stylesheet } from '@styles';

export const HallOfFame: React.FC = (): ReactElement => {

    const [honorees, setHonorees] = React.useState<Model.HallOfFame>(new Model.HallOfFame);
    const [loading, setLoading] = React.useState<boolean>(true);
    const [error, setError] = React.useState<boolean>(false);

    React.useEffect(() => {
        (async function() {
            let url ='http://www.liverpoolfcxi.co.uk/restful/hall';
            fetch(url)
                .then(res => res.json())
                .then(
                    (result) => {
                        const hall: Model.HallOfFame = Model.HallOfFame.fromJson(result);
                        setLoading(false);
                        setHonorees(hall);
                    },
                    (error) => {
                        setLoading(false);
                        setError(true);
                        console.log(error);
                    }
                )
        })();
    }, []);

    return (
        <Screen>
            <Text variant='bodyMedium' style={Stylesheet.layout.body}>These are the greatest players to have ever represented Liverpool Football Club, as voted for by YOU.</Text>
            {error && <Error />}
            {loading && <Skeleton.Team />}
            {!error && !loading && <Display.Honorees honorees={honorees} />}
        </Screen>
    );
    //<Error />;

}