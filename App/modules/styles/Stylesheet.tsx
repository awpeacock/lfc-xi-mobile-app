import { ImageStyle, StyleSheet, TextStyle, ViewStyle, Platform, Dimensions, NativeModules } from 'react-native';

import DeviceInfo from 'react-native-device-info';
import Constants from 'expo-constants';

import { Theme } from './Theme';

const {width} = Dimensions.get('screen');
const viewHeight = (Platform.OS === 'android' && Platform.Version  >= 29 && !DeviceInfo.isEmulatorSync()) ? (Dimensions.get('window').height + NativeModules.StatusBarManager.HEIGHT) : Dimensions.get('window').height;
const headerHeight = 80;
const adHeight = 70;

export const Stylesheet = StyleSheet.flatten({

    layout: {
        screen: {
            full: {
                width: '100%',
                height: (viewHeight - adHeight - NativeModules.StatusBarManager.HEIGHT)
            }as ViewStyle,
            container: {
                height: (viewHeight - headerHeight - adHeight)
            } as ViewStyle,
            scroll: {
                alignItems: 'center', 
                justifyContent: 'flex-start', 
                alignSelf: 'stretch',
                alignContent: 'stretch',
                minHeight: '100%',
            } as ViewStyle,
            content: {
                width: '100%',
                padding: 10
            } as ViewStyle,
            heading: {
                width:'100%',
                textAlign: 'center',
                color: Theme.colors.primary
            } as TextStyle
        },
        body: {
            width:'100%',
            textAlign: 'left'
        } as TextStyle,
        nav: {
            container: {
                flexDirection: 'row', 
                alignItems: 'center'
            } as ViewStyle,
            icon: { 
                common: {
                    width: 50,
                    height: 50                    
                } as ImageStyle,
                sub: {
                    marginLeft: -20 
                } as ImageStyle
            },
            title: {
                color: '#fff', 
                marginLeft: 10,
            } as TextStyle
        },
        splash: {
            container: [
                StyleSheet.absoluteFill,
                {
                    backgroundColor: Constants.expoConfig!.splash!.backgroundColor
                }
            ],
            image: {
                width: '100%',
                height: '100%',
                resizeMode: Constants.expoConfig!.splash!.resizeMode
            } as ImageStyle
        },
        home: {
            container: {
                marginHorizontal:30, 
                marginVertical: 20
            } as ViewStyle,
            icons: {
                display: 'flex',
                flexDirection: 'row', 
                justifyContent: 'space-evenly', 
                alignItems: 'center',
                width: width - 40
            } as ViewStyle
        }
    },

    components: {

        team: {
            container: {
                width: '100%',
                marginTop: 10
            } as ViewStyle,
            player: {
                container: {
                    flexDirection: 'row',
                    width: '100%'
                } as ViewStyle,
                position: {
                    width: 135,
                    borderColor: Theme.colors.outline,
                    borderWidth: 1,
                    borderRightWidth: 0,
                    borderTopLeftRadius: Theme.roundness,
                    borderBottomLeftRadius: Theme.roundness,
                    padding: 10,
                    marginBottom: 10,
                    backgroundColor: Theme.colors.surface,
                    color: '#fff'
                } as TextStyle,
                name: {
                    flex: 1,
                    borderColor: Theme.colors.outline,
                    borderWidth: 1,
                    borderLeftWidth: 0,
                    borderTopRightRadius: Theme.roundness,
                    borderBottomRightRadius: Theme.roundness,
                    padding: 10,
                    marginBottom: 10,
                } as TextStyle
            }
        },

        hall: {
            ranking: {
                common: {
                    borderColor: Theme.colors.outline,
                    borderWidth: 1,
                    padding: 10,
                    backgroundColor: Theme.colors.surface
                } as ViewStyle,
                header: {
                    borderTopLeftRadius: Theme.roundness,
                    borderTopRightRadius: Theme.roundness,
                    marginTop: 10,
                    color: '#fff'
                } as TextStyle,
                footer: {
                    borderBottomLeftRadius: Theme.roundness,
                    borderBottomRightRadius: Theme.roundness,
                } as ViewStyle,
                container: {
                    borderColor: Theme.colors.outline,
                    borderLeftWidth: 1,
                    borderRightWidth: 1,
                    padding: 5,
                    backgroundColor: '#fff'
                } as ViewStyle
            },
            player: {
                positions: {
                    marginBottom: 5
                } as TextStyle
            }
        },

        form: {

            container: {
                width: '100%'
            } as ViewStyle,

            legend: {
                marginTop: 20,
                marginBottom: 10,
                marginLeft: 5
            } as TextStyle,

            input: {
                outline: {
                    borderWidth: 2,
                    borderColor: '#000'
                },
                value: { 
                    marginBottom: 10
                }
            },

            player: {
                container: {
                    alignSelf: 'stretch'
                } as ViewStyle,
                picker: {
                    fontSize: 14,
                    padding: 0,
                    marginBottom: 10,
                    borderRadius: Theme.roundness,
                    borderWidth: 2,
                    borderColor: '#000',
                    backgroundColor: '#fff'            
                } as ViewStyle,
                placeholder: {
                    fontSize: 14,
                    paddingLeft: 15,
                    marginTop: -50,
                    marginBottom: 31,
                    color: '#aaa',
                } as TextStyle
            },
            error: {
                field: {
                    fontWeight: 'bold',
                    color: Theme.colors.error,
                    backgroundColor: Theme.colors.errorContainer,
                    borderWidth: 2,
                    borderColor: Theme.colors.error,
                    borderRadius: Theme.roundness,
                    padding: 10,
                    marginTop: -5,
                    marginBottom: 10
                } as TextStyle,
                pointer: {
                    triangle: {
                        width: 0,
                        height: 0,
                        backgroundColor: 'transparent',
                        borderStyle: 'solid',
                        borderLeftWidth: 8,
                        borderRightWidth: 8,
                        borderBottomWidth: 16,
                        borderLeftColor: 'transparent',
                        borderRightColor: 'transparent',
                        marginLeft: 20
                    } as ViewStyle,
                    outline: {
                        zIndex: 1,
                        marginTop: -20,
                        borderBottomColor: Theme.colors.error
                    } as ViewStyle,
                    fill: {
                        zIndex: 2,
                        marginTop: -12,
                        borderBottomColor: Theme.colors.errorContainer
                    } as ViewStyle
                }
            },
            spinner: {
                marginVertical: 25
            } as ViewStyle

        },

        skeleton: {
            rank: {
                marginVertical: 10
            } as ViewStyle,
            players: {
                width: '100%',
                marginBottom: 10
            } as ViewStyle
        },

        links: {
            hero: {
                container: {
                    width: width-40,
                    marginTop: 20
                } as ViewStyle,
                image: {
                    width: '100%',
                    height: 100, 
                    borderBottomLeftRadius: 0, 
                    borderBottomRightRadius: 0
                } as ImageStyle,
                button: {
                    paddingBottom: 0,
                    fontSize: 16,
                    fontWeight:800
                } as ViewStyle
            },
            icon: {
                container: {
                    alignItems: 'center'
                } as ViewStyle,
                image: {
                    width: ((width - 120)/2), 
                    height: ((width - 120)/2),
                    marginTop: 20,
                    borderColor: '#a00',
                    borderWidth: 1,
                    borderRadius: ((width - 60)/4)
                } as ImageStyle,
                button: {
                    width: ((width - 120)/2),
                    marginTop: -(6+((width - 120)/16)),
                    borderRadius: 5
                } as ViewStyle,
                text: {
                    fontSize: 10,
                    marginHorizontal: 5,
                    marginVertical: 1
                } as TextStyle
            }
        },

        ad: {
            width: '100%',
            height: 70,
            justifyContent: 'center',
            alignItems: 'center',
            borderTopColor: '#ccc',
            borderTopWidth: 1,
            backgroundColor: 'rgba(255,255,255,0.5)'
        } as ViewStyle,

        status: {
            container: {
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 20,
                borderWidth: 2,
                padding: 10
            } as TextStyle,
            text: {
                width: (width - 110),
                marginLeft: 5,
                fontWeight: 'bold' as 'bold'
            } as TextStyle,
            success: {
                borderColor: Theme.colors.success,
                backgroundColor: Theme.colors.successContainer,
                color: Theme.colors.success
            } as TextStyle,
            error: {
                borderColor: Theme.colors.error,
                backgroundColor: Theme.colors.errorContainer,
                color: Theme.colors.error
            } as TextStyle,
            info: {
                borderColor: Theme.colors.info,
                backgroundColor: Theme.colors.infoContainer,
                color: Theme.colors.info
            } as TextStyle,
        },
        error: {
            container: {
                width: '100%',
                alignItems: 'center'
            } as ViewStyle,
            image: {
                width: 260,
                height: 380,
                marginBottom: -60
            } as ImageStyle
        }
        
    }

});