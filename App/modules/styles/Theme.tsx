import {DefaultTheme, configureFonts} from 'react-native-paper';

const fontConfig = {

    // Main headings on screens
    displayLarge: {
        ...DefaultTheme.fonts.displayLarge,
        fontSize: 32,
        fontWeight: 'bold' as 'bold'
    },

    // Nav header
    titleLarge: {
        ...DefaultTheme.fonts.titleLarge,
        fontSize: 18,
        fontWeight: 'bold' as 'bold'
    },

    // Used for form legends
    titleMedium: {
        ...DefaultTheme.fonts.titleMedium,
        fontSize: 16,
        fontWeight: 'bold' as 'bold'
    },

    // Usual body text
    bodyMedium: {
        ...DefaultTheme.fonts.bodyMedium,
        fontSize: 14
    }

};

export const Theme = {

    ...DefaultTheme,

    colors: {
        ...DefaultTheme.colors,
        primary: '#a00',
        surface: '#a00',
        outline: '#a00',
        secondaryContainer: '#e0aaa6',
        onSecondaryContainer: '#a00',

        // Special one for Navigator
        background: 'rgba(255,255,255,0.5)',

        // Status Message
        success: '#0a0',
        successContainer: '#d0f2d1',
        error: '#a00',
        errorContainer: '#e0aaa6',
        info: '#db7b2b',
        infoContainer: '#edeccc'    
    },
    roundness: 10,
    dark: false,
    fonts: configureFonts({config: fontConfig})

};