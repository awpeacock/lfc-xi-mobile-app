const { getDefaultConfig: getExpoDefaultConfig } = require('expo/metro-config');
const { getDefaultConfig, mergeConfig } = require('@react-native/metro-config');

const path = require('path');
const packages = [path.resolve(__dirname, '../Common'), path.resolve(__dirname, '../Common/src'), path.resolve(__dirname, '../Common/src/model')];
const config = {
    resolver: {
        enableGlobalPackages: true,
        nodeModulesPaths: packages
    },
    watchFolders: packages
};

module.exports = mergeConfig(getDefaultConfig(__dirname), getExpoDefaultConfig(__dirname), config);