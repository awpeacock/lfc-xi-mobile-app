import { Formation } from './Formation';
import { Team } from './Team';

export class Submission {

    team: Team;
    email: string;

    errors: Record<string, string>;

    constructor(formation: Formation) {
        this.team = new Team(formation);
        this.email = '';
        this.errors = {};
    }

    async save(): Promise<boolean> {
        //console.log('Saving "'+this.team.name+'" for '+this.email);
    
        const url: string ='http://www.liverpoolfcxi.co.uk/restful/submit';
        let params: Array<string> = [];
        params.push('name' + '=' + encodeURIComponent(this.team.name));
        params.push('email' + '=' + encodeURIComponent(this.email));
        for ( let p = 0; p < this.team.size(); p++ ) {
            const id = this.team.lineup[p].player.id ?? 0;
            params.push('players['+p+']' + '=' + id);
        }
        
        const data: string = params.join("&");
        let success: boolean = false;
        await fetch(url, { 
            method: 'POST',
            headers: new Headers({
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
            }),
            body: data,
        })
            .then(res => res.json())
            .then(
                (result) => {
                    if ( result.errors != null ) {
                        result.errors.forEach((e: Record<string, Array<Record<string, string>>>) => {
                            for (const [key, messages] of Object.entries(e)) {
                                this.errors[key.replace(/_(\d+?)$/, '[$1]').replace('_', '').replace('user', 'email').replace('players', 'lineup')] = messages[0].message;
                            }
                        });
                        success = false;
                    } else {
                        this.team = Team.fromJson(result);
                        success = true;
                    }
                },
                (error) => {
                    throw 'Sorry, something went wrong - this feels like a real own goal! Please try again later.';
                }
            )
        return success;

    }

}