import { Nullable } from './Nullable';

//TODO: Replace with correct JSON (in its own namespace) when web services refactored
interface Json {
    'id': number,
    'name': string
}

export class Player {

    id: Nullable<number>;
    forename: string;
    surname: string;
    positions: Array<string>;

    constructor(forename?: string, surname?: string) {
        this.id = null;
        this.forename = (forename ? forename : '');
        this.surname = (surname ? surname: '');
    }

    toString(): string {
        return this.forename + ' ' + this.surname;
    }

    static fromJson(json: Json): Player {
        const name = [json.name.substring(0, json.name.indexOf(' ')), json.name.substring(json.name.indexOf(' '))];
        const player = new Player(name[0], name[1]);
        player.id = json.id;
        return player;
    }

}