export class Config {

    static titles: Record<string, string> = {
        'submission': 'Submit Your Team',
        'month': 'Team of the Month',
        'hall': 'Hall of Fame'
    };

}