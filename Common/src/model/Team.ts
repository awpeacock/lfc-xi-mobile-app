import { Player } from './Player';
import { Position } from './Position';
import { Selection } from './Selection';
import { Formation } from './Formation';

export class Team {

    id: number;
    name: string;
    created: Date;
    formation: Formation;
    lineup: Array<Selection> = [];

    constructor(formation: Formation, name?: string) {
        this.name = name ? name : '';
        this.formation = formation;
        for ( let p = 0; p < this.formation.positions.length; p++ ) {
            this.lineup.push(new Selection(this.formation.positions[p], new Player()));
        }
        
    }

    add(position: Position.ID, player: Player) {
        for ( let p = 0; p < this.formation.positions.length; p++ ) {
            if ( this.lineup[p].position == position && (this.lineup[p].player.id == null && this.lineup[p].player.forename == '' && this.lineup[p].player.surname == '') ) {
                this.lineup[p].player = player;
                return;
            }
        }
        this.lineup.push(new Selection(position, player));
    }

    size(): number {
        return this.lineup.length;
    }

    static fromJson(json): Team {
        const team: Team = new Team(Formation.fromJson(json.formation), json.name);
        team.created = new Date();
        if ( json.created.length == 6 ) {
            team.created.setFullYear(json.created.substring(0,4));
            team.created.setMonth(json.created.substring(4)-1);
            team.created.setDate(1);
            team.created.setHours(12);
            team.created.setMinutes(0);
            team.created.setSeconds(0);
        } else {
            team.created.setTime(json.created);
        }
        for ( let p = 0; p < json.players.length ; p++ ) {
            const player: Player = new Player(json.players[p].forename, json.players[p].surname);
            const position: Position.ID = json.players[p].position.initials as Position.ID;
            team.add(position, player);
        }
        return team;
    }

}