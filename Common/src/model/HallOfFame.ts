import { Player } from './Player';

namespace Json {
    export interface Ranking {
        'rank': number,
        'votes': number,
        'players': Array<Player>
    }
    export interface Player {
        'name': string,
        'positions': string
    }
}

export class HallOfFame {

    rankings: Array<Ranking> = [];

    static fromJson(json: Array<Json.Ranking>): HallOfFame {
        let hall: HallOfFame = new HallOfFame();
        for ( let r = 0; r < json.length; r++ ) {
            let players: Array<Player> = [];
            for ( let p = 0; p < json[r].players.length; p++ ) {
                const name: string = json[r].players[p].name;
                const positions: Array<string> = json[r].players[p].positions.split(', ');
                const player: Player = new Player(name.substring(0, name.lastIndexOf(' ')), name.substring(name.lastIndexOf(' ')+1));
                player.positions = positions;
                players.push(player);
            }
            hall.rankings.push(new Ranking(json[r].rank, json[r].votes, players));
        }
        return hall;
    } 

}

class Ranking {

    rank: number;
    votes: number;
    players: Array<Player>;

    constructor(rank: number, votes: number, players: Array<Player>) {
        this.rank = rank;
        this.votes = votes;
        this.players = players;
    }

}