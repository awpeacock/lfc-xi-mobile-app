import { Player } from './Player';
import { Position } from './Position';

export class Selection {

    position: Position.ID;
    player: Player;

    constructor(position: Position.ID, player: Player) {
        this.position = position;
        this.player = player;
    }
}