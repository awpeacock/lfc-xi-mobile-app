import { Position } from './Position';

//TODO: Replace with correct JSON (in its own namespace) when web services refactored
namespace Json {
    export interface Formation {
        'name': string,
        'positions'
    }
    export interface Position {
        'id': string,
        'title': string
    }
}

export class Formation {

    name: string;
    positions: Array<Position.ID> = [];

    constructor(name: string) {
        this.name = name;
    }

    add(position: Position.ID): void {
        this.positions.push(position);
    }

    static fromJson(json: Json.Formation): Formation {
        // Until we tidy up the JSON
        if ( !json.name ) {
            json = {name: '4-4-2', positions: [{initials: 'GK'}, {initials: 'DL'}, {initials: 'DC'}, {initials: 'DC'}, {initials: 'DR'}, {initials: 'ML'}, {initials: 'MC'}, {initials: 'MC'}, {initials: 'MR'}, {initials: 'S'}, {initials: 'S'}]}
        }
        const formation = new Formation(json.name);
        for ( let p = 0; p < json.positions.length ; p++ ) {
            formation.add(json.positions[p].initials as Position.ID);
        }
        return formation;
    }
    
}
