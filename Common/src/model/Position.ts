import { Player } from './Player';

export namespace Position {

    export enum ID {
        GK = 'Goalkeeper',
        DL = 'Left Back',
        DR = 'Right Back',
        DC = 'Centre Back',
        ML = 'Left Wing',
        MR = 'Right Wing',
        MC = 'Centre Midfield',
        S = 'Striker'
    }

    export class Group {
        id: Position.ID;
        players: Array<Player> = [];
    
        constructor(position: Position.ID) {
            this.id = position;
        }
    
        add(player: Player): void {
            this.players.push(player);
        }
    }
    
}