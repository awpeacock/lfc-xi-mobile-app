module.exports = {

    rootDir: ".",
    roots: [
        "<rootDir>"
    ],

    preset: "ts-jest",

    transform: {
        "^.+\\.(t|j)s?$": "ts-jest"
    },
    transformIgnorePatterns: [],

    testEnvironment: 'node',

    moduleFileExtensions: [
        "ts",
        "tsx",
        "js"
    ],
    moduleDirectories: [
        "node_modules",
        __dirname
    ],
    moduleNameMapper: {
        "^@models": "<rootDir>/src/models"
    },
    modulePaths: [
        "node_modules", 
        "<rootDir>/src"
    ]
}